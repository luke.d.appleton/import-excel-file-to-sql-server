import sys
import pandas as pd
import sqlalchemy
import pyodbc

#use pandas to import excel file
df = pd.read_excel( sys.argv[1] , sheet_name='Data')

#use SQLalchemy to establish connection to local db instance
engine = sqlalchemy.create_engine('mssql+pyodbc://(localdb)\\LocalDBDemo/master?driver=ODBC+Driver+13+for+SQL+Server')

#Use pandas to export the dataframe to MS SQL server
df.to_sql("dbo.aggregated", engine , index=False)